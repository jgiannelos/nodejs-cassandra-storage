# NodeJS cassandra storage

## Description

A Cassandra DB storage abstraction to be used as a backend in nodejs projects.
This library also provides an Express JS middleware to easily store/cache HTTP responses.

## Usage

Install from main git branch:

```
> npm install https://gitlab.wikimedia.org/repos/content-transform/nodejs-cassandra-storage/-/archive/main/nodejs-cassandra-storage-main.tar.gz
```

### Local development environment

Dependencies:

When using local nodejs for development:

- nodejs (CI is using node18)
- docker/docker-compose (required for backing cassandra instance)

When using a fully docker based setup:

- docker/docker-compose

#### Local nodejs based setup

- Start development cassandra instance (initialization might take some time)

```
> docker compose up -d cassandra
# Wait until cassandra STATUS is healthy (DB is ready for connections)
> docker compose ps cassandra
```

- Install js dependencies

```
> npm install
```

- Run linter

```
> npm run lint
```

- Run automatic linting/formatting fixes

```
> npm run lint:fix
```

- Run tests

```
> npm run tests
```

#### Docker based setup

- Get shell access to a nodejs container:

```
> docker compose -it run tests bash
```

Then you can use the same npm commands as described in the previous section.

- Running the tests suite

```
> docker compose run tests
```

### Middleware

An example of a simple ExpressJS app using the storage middleware can be found under `./examples/middleware`.

### JSDocs

To generate the API documentation:

```
> npm run env -- jsdoc -c ./jsdoc.json  lib
```

HTML output is rendered in `./out`
